const express = require('express');
const router = express.Router();
const auth = require('./middlewares/auth')
const SessionController = require('./controllers/SessionController');

router.post('/authenticate', SessionController.authenticate);
router.post('/register', SessionController.register);

router.use(auth);
router.post('/authenticaded', SessionController.authenticated);

module.exports = router;