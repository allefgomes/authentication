const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth.json')

generateToken = (params = { }) => { 
  return jwt.sign(params, authConfig.secret, { 
    expiresIn: 86400
  })
}

module.exports = {
  async register (req, res) {
    try { 
      const user = await User.create(req.body);
      user.password = undefined

      res.send({ user, token: generateToken({ id: user.id }) });
    } catch (err) {
      res.status(400).send({ err })
    }   
  },
  async authenticate (req, res) {
    const { email, password } = req.body;

    const response = await User.findOne({ email })
    
    if (!response) 
      return res.status(400).send({ error: "User not found" })

    const user = response
    if (!await bcrypt.compare(password, user.password))
      return res.status(400).send({ error: "Invalid password" })

    user.password = undefined

    res.send({ 
      user, 
      token: generateToken({ id: user.id }) 
    })
  },
  async authenticated (req, res) {
    res.status(400).send({ mensage: "User is Logged!" })
  }
};